package javafiles.Home;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class HomeWindow extends Application{

    public void start(Stage prStage){
        prStage.setTitle("MyProject");
        prStage.setHeight(500);
        prStage.setWidth(700);
        prStage.setResizable(true);
        prStage.getIcons().add(new Image("asstes/image/demo.jpg"));
        prStage.setResizable(true);
        prStage.setX(0);
        prStage.setY(0);
        prStage.setWidth(1800);
        prStage.setHeight(1000);

        Text tx=new Text(10,30,"Good Evening");
        tx.setFont(new Font(40));
        tx.setFill(Color.YELLOW);

        Text tx2=new Text(1000,30,"Have A Nice Day");
        tx2.setFont(new Font(20));
        tx2.setFill(Color.RED);


        Text java=new Text("Java");
        java.setFont(new Font(40));
        java.setFill(Color.CHOCOLATE);

        
        Text Python=new Text("Python");
        Python.setFont(new Font(40));
        Python.setFill(Color.WHITE);
        

        
        Text Cpp=new Text("Cpp");
        Cpp.setFont(new Font(40));

        Text web=new Text(10,10,"web");
        web.setFont(new Font(40));
        web.setFill(Color.YELLOW);

        Text backend=new Text(10,10,"backend");
        backend.setFont(new Font(40));

        Text App=new Text(10,10,"App");
        App.setFont(new Font(40));

        

        VBox vb=new VBox(40,web,backend,App);
        vb.setLayoutX(70);
        vb.setLayoutY(100);


        VBox vb1=new VBox(40,java,Python,Cpp);
        vb1.setLayoutX(1000);
        vb1.setLayoutY(700);

        HBox hb=new HBox(40,vb,vb1);
        hb.setAlignment(Pos.CENTER);
        hb.setLayoutX(300);
        hb.setLayoutY(100);

        Group group=new Group(hb,tx,tx2);
        Scene sc=new Scene(group,450,100);
        sc.setFill(Color.AQUA);
        prStage.setScene(sc);
        prStage.show();
    }
        
    
}
