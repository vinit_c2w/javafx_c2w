package javafiles;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class assi2button extends Application {

     public void start(Stage prStage){
        prStage.setTitle("Assi3");
        prStage.setHeight(500);
        prStage.setWidth(700);
        prStage.setResizable(true);


        Label l1= new Label("Coe2Web.in");
        l1.setPadding(new Insets(100,0,100,0));
        Font ft = Font.font("Courier new", FontWeight.BOLD, 15);

        // Button b1=new Button("Hello Super-X");
        // b1.setPrefWidth(200);
        // b1.setFont(ft);
        // b1.setOnAction(new EventHandler<ActionEvent>() {
        //     public void handle(ActionEvent event){
        //         System.out.println("Super-x 2024");
        //     }
        // });
                

                Button b2=new Button("java");
                b2.setPrefWidth(200);
                b2.setFont(ft);

                b2.setStyle("-fx-background-color:BLUE");
                b2.setOnAction(new EventHandler<ActionEvent>() {
                    public void handle(ActionEvent event){
                        System.out.println("Java");
                        b2.setStyle("-fx-background-color:GREEN");
                    }
                });

                Button b3=new Button("Core2web-Super-x");
                b3.setPrefWidth(200);
                b3.setFont(ft);

                b3.setStyle("-fx-background-color:BLUE");
                b3.setOnAction(new EventHandler<ActionEvent>() {
                    public void handle(ActionEvent event){
                        System.out.println("super-x");
                        b3.setStyle("-fx-background-color:GREEN");
                    }
                });

                Button b4=new Button("Core2-DSA");
                b4.setPrefWidth(200);
                b4.setFont(ft);

                b4.setStyle("-fx-background-color:BLUE");
                b4.setOnAction(new EventHandler<ActionEvent>() {
                    public void handle(ActionEvent event){
                        System.out.println("DSA");
                        b4.setStyle("-fx-background-color:GREEN");

                    }
                });
                

                VBox vb=new VBox(10,l1,b2,b3,b4);
                vb.setAlignment(Pos.CENTER);
                vb.setStyle("-fx-background-color:YELLOW");
                

                Scene sc=new Scene(vb,300,200,Color.YELLOW);
                prStage.setScene(sc);
        
                prStage.show();
     }
    
}
