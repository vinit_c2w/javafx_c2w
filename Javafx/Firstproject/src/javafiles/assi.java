package javafiles;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class assi extends Application {

     public void start(Stage prStage){
        prStage.setTitle("Assignment 1");
        prStage.setHeight(1000);
        prStage.setWidth(1000);
        prStage.setX(50);
        prStage.setY(20);
        prStage.setResizable(true);
        prStage.show();

        BorderPane bp=new BorderPane();

        Label txt1=new Label("Top left");
        txt1.setFont(new Font(30));

        Label txt2=new Label("Top Right");
        txt2.setFont(new Font(30));
        


        Label txt3=new Label("Center");
        txt3.setFont(new Font(30));
        

        Label txt4=new Label("Bottom");
        txt4.setFont(new Font(30));

        Label txt5=new Label("Left Bottom");
        



        bp.setBottom(txt4);
        bp.setRight(txt2);
        bp.setLeft(txt1);
        bp.setCenter(txt3);
        
        
        
        
        Scene sc=new Scene(bp,300,200);
        prStage.setScene(sc);
        


     }
    
}
