package javafiles;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class imageview extends Application{

    public void start(Stage prStage){
        prStage.setTitle("Image View");
        prStage.setHeight(500);
        prStage.setWidth(700);
        prStage.setResizable(true);


        Text txt1=new Text("JAVA");
        txt1.setFont(new Font(40));

         ImageView im=new ImageView("asstes/image/java1.png");
         im.setFitWidth(150);
         im.setFitHeight(150);
         im.setPreserveRatio(true);
         im.setLayoutX(50);
         im.setLayoutY(200);
         

         
          
         Text txt2=new Text("Python");
         txt2.setFont(new Font(40));

          ImageView im2=new ImageView("asstes/image/python.jpg");
          im2.setFitWidth(150);
         im2.setFitHeight(150);
         im2.setPreserveRatio(true);
          

         
          
         
         
         Text txt3=new Text("JS");
        txt3.setFont(new Font(40));
        
        ImageView im3=new ImageView("asstes/image/JS.jpg");
         im3.setFitWidth(130);
         im3.setFitHeight(150);
         im3.setPreserveRatio(true);



        Label react=new Label("REACT");
        react.setFont(new Font(40));
        react.setMinWidth(40);


        ImageView imr=new ImageView("asstes/image/react.jpg");
        imr.setFitHeight(100);
        imr.setFitWidth(100);
        imr.setPreserveRatio(true);


        
           Text txt4=new Text("Fultter");
            txt4.setFont(new Font(30));


           ImageView im4=new ImageView("asstes/image/Fultter.png");
           im4.setFitWidth(150);
           im4.setFitHeight(200);
           im4.setPreserveRatio(true);


           VBox vb=new VBox(20);
           vb.setMinHeight(500);
           vb.setLayoutX(0);
           vb.setLayoutY(0);
           vb.setMaxWidth(400);

           Text txtn=new Text("NODEJS");
           txtn.setFont(new Font(40));

           ImageView imn=new ImageView("asstes/image/nodejs.png");
           imn.setFitWidth(150);
           imn.setFitHeight(200);
           imn.setPreserveRatio(true);




        
            

        HBox hb1=new HBox(60,im,txt1,imr,react);
        hb1.setLayoutX(30);
        hb1.setLayoutY(130);
        hb1.setMinWidth(0);
        hb1.setStyle("-fx-background-Color:ORANGE");

        HBox hb2=new HBox(100,im2,txt2,im4,txt4);
        hb2.setLayoutX(30);
        hb2.setLayoutY(130);
        hb2.setMinWidth(0);
        hb2.setStyle("-fx-background-Color:YELLOW");

        HBox hb3=new HBox(120,im3,txt3,imn,txtn);
        hb3.setLayoutX(30);
        hb3.setLayoutY(370);
        hb3.setMinWidth(150);
        hb3.setStyle("-fx-background-Color:RED");




            


         Group gr=new Group(hb1,hb2,hb3,vb);
         Scene sc=new Scene(gr);
         prStage.setScene(sc);
         sc.setFill(Color.AQUA);

        prStage.show();

    }
    
}
