package javafiles;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class assi1 extends  Application {

  //  private static final Node Web = null;

    public void start(Stage prStage){

        prStage.setTitle("Assignment1");
        prStage.setResizable(true);
        prStage.setHeight(1000);
        prStage.setWidth(1000);
        prStage.setX(0);
        prStage.setY(0);

        Image image=new Image("asstes/image/demo.jpg");
        ImageView imageView=new ImageView(image);
        imageView.setFitWidth(300);
        imageView.setPreserveRatio(true);

       

        Label lb=new Label("      WEB PAGE");
        lb.setFont(new Font(40));
        lb.setAlignment(Pos.CENTER);
        lb.setPrefHeight(200);

        VBox vb = new VBox(lb);
        

        HBox hb=new HBox(15,imageView,vb);
        hb.setPrefHeight(200);
        hb.setPrefWidth(650);
        hb.setStyle("-fx-background-color:AQUA");
        
        Group gr=new Group(hb);

        Scene sc=new Scene(gr,1000,700,Color.TEAL);
        prStage.setScene(sc);
        prStage.show();

    }
    
}
